package com.multimoney.multimoney.presentation.ui.home.product

enum class OfferAndTipType(val type: String) {
    TIP("tip"),
    OFFER_SMART("offer_smart"),
    OFFER_CRYPTO("offer_crypto"),
    OFFER_CREDIT_EXTENSION("offer_credit_extension")
}