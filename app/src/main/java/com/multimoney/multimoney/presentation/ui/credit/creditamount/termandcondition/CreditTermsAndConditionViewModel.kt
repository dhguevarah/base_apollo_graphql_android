package com.multimoney.multimoney.presentation.ui.credit.creditamount.termandcondition

import com.multimoney.multimoney.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CreditTermsAndConditionViewModel @Inject constructor() : BaseViewModel()