package com.multimoney.multimoney.presentation.ui.login.signup.personaldata

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.viewModelScope
import com.multimoney.data.util.catalog.Brand
import com.multimoney.domain.interaction.security.QueryDataInformationClientUseCase
import com.multimoney.domain.model.security.ClientInfoCr
import com.multimoney.domain.model.util.onFailure
import com.multimoney.domain.model.util.onLoading
import com.multimoney.domain.model.util.onSuccess
import com.multimoney.multimoney.R
import com.multimoney.multimoney.presentation.base.BaseViewModel
import com.multimoney.multimoney.presentation.ui.login.signup.personaldata.SignUpPersonalDataViewModel.BaseEvent.OnFormValidateCompleted
import com.multimoney.multimoney.presentation.ui.login.signup.personaldata.SignUpPersonalDataViewModel.UIEvent.OnFirstLastNameChange
import com.multimoney.multimoney.presentation.ui.login.signup.personaldata.SignUpPersonalDataViewModel.UIEvent.OnFirstNameChange
import com.multimoney.multimoney.presentation.ui.login.signup.personaldata.SignUpPersonalDataViewModel.UIEvent.OnIdentificationTypeValueChange
import com.multimoney.multimoney.presentation.ui.login.signup.personaldata.SignUpPersonalDataViewModel.UIEvent.OnIdentificationValueChange
import com.multimoney.multimoney.presentation.ui.login.signup.personaldata.SignUpPersonalDataViewModel.UIEvent.OnNationalityChange
import com.multimoney.multimoney.presentation.ui.login.signup.personaldata.SignUpPersonalDataViewModel.UIEvent.OnNextActionClick
import com.multimoney.multimoney.presentation.ui.login.signup.personaldata.SignUpPersonalDataViewModel.UIEvent.OnSecondLastNameChange
import com.multimoney.multimoney.presentation.ui.login.signup.personaldata.SignUpPersonalDataViewModel.UIEvent.OnSecondNameChange
import com.multimoney.multimoney.presentation.ui.login.signup.personaldata.SignUpPersonalDataViewModel.UIEvent.OnStart
import com.multimoney.multimoney.presentation.ui.login.signup.personaldata.SignUpPersonalDataViewModel.UIEvent.OnValidateDocument
import com.multimoney.multimoney.presentation.util.CrDocuments
import com.multimoney.multimoney.presentation.util.Nationalities.CostaRicaDimex
import com.multimoney.multimoney.presentation.util.Nationalities.CostaRicaId
import com.multimoney.multimoney.presentation.util.Nationalities.ElSalvador
import com.multimoney.multimoney.presentation.util.Nationalities.Guatemala
import com.multimoney.multimoney.presentation.util.validDui
import com.multimoney.multimoney.presentation.util.validId
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignUpPersonalDataViewModel @Inject constructor(
    private val queryDataInformationClientUseCase: QueryDataInformationClientUseCase
) : BaseViewModel() {
    // UIState
    var uiState by mutableStateOf(UIState())
        private set

    var closeKeyboard by mutableStateOf(false)

    // Interactions
    var onSuccessDataInformationClient by mutableStateOf<ClientInfoCr?>(null)

    // Stateless
    var documentLength = 0

    private fun isFormValid() = emitBaseEvent(
        OnFormValidateCompleted(
            when (uiState.nationalityValue) {
                ElSalvador.country -> uiState.personalDocumentValue.isNotBlank() && (uiState.personalDocumentValue.length == ElSalvador.documentSize) && !uiState.personalIdError.first && uiState.firstNameValue.isNotBlank() && uiState.firstLastNameValue.isNotBlank()
                Guatemala.country -> uiState.personalDocumentValue.isNotBlank() && (uiState.personalDocumentValue.length == Guatemala.documentSize) && !uiState.personalIdError.first && uiState.firstNameValue.isNotBlank() && uiState.firstLastNameValue.isNotBlank()
                CostaRicaId.country -> uiState.personalDocumentValue.isNotBlank() &&
                        (uiState.personalDocumentValue.length == CostaRicaId.documentSize || uiState.personalDocumentValue.length == CostaRicaDimex.documentSize) &&
                        !uiState.personalIdError.first && uiState.identificationValueType.isNotBlank() && onSuccessDataInformationClient?.fullName != null
                else -> false
            }
        )
    )

    private fun getNationality(country: String) = when (country) {
        ElSalvador.country -> ElSalvador.name
        Guatemala.country -> Guatemala.name
        else -> CostaRicaId.name
    }

    private fun getCountry(nationality: String) = when (nationality) {
        ElSalvador.name -> ElSalvador.country
        Guatemala.name -> Guatemala.country
        CostaRicaId.name -> CostaRicaId.country
        else -> ""
    }

    private fun getDocumentLength(documentType: String) {
        documentLength = when (documentType) {
            ElSalvador.documentType -> {
                ElSalvador.documentSize
            }
            Guatemala.documentType -> {
                Guatemala.documentSize
            }
            CostaRicaDimex.documentType -> {
                CostaRicaDimex.documentSize
            }
            CostaRicaId.documentType -> {
                CostaRicaId.documentSize
            }
            else -> {
                0
            }
        }
    }

    private fun validateCrDocument(
        user: String,
    ): Pair<Boolean, Int> {
        val status = validId(
            if (uiState.identificationValueType == CrDocuments.IdDocument.document) CostaRicaId.documentSize else CostaRicaDimex.documentSize,
            R.string.sign_up_personal_data_id_not_valid,
            uiState.personalDocumentValue.length
        )
        if (status.first.not()) {
            closeKeyboard = true
            callQueryDataInformationClient(uiState.personalDocumentValue, Brand.Revamp.id, user)
        } else {
            if (onSuccessDataInformationClient?.fullName.isNullOrBlank().not()) {
                onSuccessDataInformationClient = null
            }
        }
        return status
    }

    private fun callQueryDataInformationClient(
        identification: String,
        idBrant: Int,
        user: String,
    ) {
        viewModelScope.launch {
            queryDataInformationClientUseCase.invoke(
                identification,
                idBrant,
                user
            ).collectLatest { result ->
                result.onSuccess {
                    onSuccessDataInformationClient = it
                    isLoading = false
                    isFormValid()
                }
                result.onFailure {
                    isLoading = false
                    onSuccessDataInformationClient = null
                    uiState = uiState.copy(
                        personalIdError = Pair(
                            true,
                            R.string.sign_up_personal_data_id_not_valid
                        )
                    )
                    isFormValid()
                }
                result.onLoading {
                    isLoading = true
                }
            }
        }
    }

    private fun onNationalityChange(
        nationality: String,
        updateNationality: (nationality: String) -> Unit
    ) {
        uiState = uiState.copy(nationalityValue = nationality, personalDocumentValue = "")
        cleanUI()
        updateNationality.invoke(getNationality(nationality))
    }

    private fun onStart(
        nationality: String,
        identificationValue: String,
        identificationType: String,
        firstName: String,
        secondName: String,
        firstLastName: String,
        secondLastName: String,
        fullName: String
    ) {
        uiState = uiState.copy(
            nationalityValue = getCountry(nationality),
            identificationValueType = identificationType,
            personalDocumentValue = identificationValue,
            firstNameValue = firstName,
            secondNameValue = secondName,
            firstLastNameValue = firstLastName,
            secondLastNameValue = secondLastName,
            fullNameValue = fullName
        )
        onSuccessDataInformationClient?.fullName = fullName
        isFormValid()
    }

    private fun cleanUI() {
        uiState = uiState.copy(
            identificationValueType = "",
            personalIdError = Pair(false, R.string.sign_up_personal_data_id_sv_required),
            nameError = Pair(false, R.string.sign_up_personal_data_id_sv_required),
            lastNameError = Pair(false, R.string.sign_up_personal_data_id_sv_required),
            personalDocumentValue = "",
            firstNameValue = "",
            secondNameValue = "",
            firstLastNameValue = "",
            secondLastNameValue = "",
            closeKeyboard = false
        )
    }

    private fun onIdentificationTypeValueChange(documentType: String) {
        getDocumentLength(documentType)
        uiState = uiState.copy(identificationValueType = documentType)
    }

    private fun onIdentificationValueChange(
        identificationValue: String,
        identificationShareViewModelChange: () -> Unit
    ) {
        if (identificationValue.length <= documentLength) {
            uiState = uiState.copy(personalDocumentValue = identificationValue)
            identificationShareViewModelChange()
        }
    }

    private fun onFirstNameValueChange(
        firstName: String,
        onSharedViewModelFirstNameChange: () -> Unit
    ) {
        uiState = uiState.copy(firstNameValue = firstName)
        onSharedViewModelFirstNameChange.invoke()
        isFormValid()
    }

    private fun onSecondNameValueChange(
        secondName: String,
        onSharedViewModelSecondNameChange: () -> Unit
    ) {
        uiState = uiState.copy(secondNameValue = secondName)
        onSharedViewModelSecondNameChange.invoke()
        isFormValid()
    }

    private fun onFirstLastNameValueChange(
        firstLastName: String,
        onSharedViewModelFirstLastNameChange: () -> Unit
    ) {
        uiState = uiState.copy(firstLastNameValue = firstLastName)
        onSharedViewModelFirstLastNameChange.invoke()
        isFormValid()
    }

    private fun onSecondLastNameValueChange(
        secondLastName: String,
        onSharedViewModelSecondLastNameChange: () -> Unit
    ) {
        uiState = uiState.copy(secondLastNameValue = secondLastName)
        onSharedViewModelSecondLastNameChange.invoke()
        isFormValid()
    }

    fun getFullName(): String =
        "${uiState.firstNameValue} ${uiState.secondNameValue} ${uiState.firstLastNameValue} ${uiState.secondLastNameValue}"


    private fun onNextActionClick(
        onUserDataValueChange: () -> Unit,
        onCallMutationUpdateUserRegisterUseCase: () -> Unit
    ) {
        onUserDataValueChange()
        onCallMutationUpdateUserRegisterUseCase()
    }

    private fun validateDocument(email: String?) {
        uiState = uiState.copy(
            personalIdError = when (uiState.nationalityValue) {
                CostaRicaId.country -> validateCrDocument(email ?: "")
                ElSalvador.country -> validDui(uiState.personalDocumentValue)
                else -> validId(
                    Guatemala.documentSize,
                    R.string.sign_up_personal_data_dpi_gt_not_valid,
                    uiState.personalDocumentValue.length
                )
            }
        )
        isFormValid()
    }

    data class UIState(
        val nationalityValue: String = "",
        val personalDocumentValue: String = "",
        val identificationValueType: String = "",
        val firstNameValue: String = "",
        val secondNameValue: String = "",
        val firstLastNameValue: String = "",
        val secondLastNameValue: String = "",
        val fullNameValue: String = "",
        val personalIdError: Pair<Boolean, Int> = Pair(
            false,
            R.string.sign_up_personal_data_id_sv_required
        ),

        val nameError: Pair<Boolean, Int> = Pair(false, 0),
        val lastNameError: Pair<Boolean, Int> = Pair(false, 0),
        val closeKeyboard: Boolean = false
    )

    fun onUIEvent(event: UIEvent) {
        when (event) {
            is OnStart -> onStart(
                event.nationality,
                event.identificationValue,
                event.identificationType,
                event.firstName,
                event.secondName,
                event.firstLastName,
                event.secondLastName,
                event.fullName
            )
            is OnNationalityChange -> onNationalityChange(
                event.nationality,
                event.updateNationality
            )
            is OnFirstNameChange -> onFirstNameValueChange(
                event.firstName,
                event.onSharedViewModelFirstNameChange
            )
            is OnSecondNameChange -> onSecondNameValueChange(
                event.secondName,
                event.onSharedViewModelSecondNameChange
            )
            is OnFirstLastNameChange -> onFirstLastNameValueChange(
                event.firstLastName,
                event.onSharedViewModelFirstLastNameChange
            )
            is OnSecondLastNameChange -> onSecondLastNameValueChange(
                event.secondLastName,
                event.onSharedViewModelSecondLastNameChange
            )
            is OnIdentificationTypeValueChange -> onIdentificationTypeValueChange(event.identificationType)
            is OnIdentificationValueChange -> onIdentificationValueChange(
                event.identification,
                event.identificationShareViewModelChange
            )
            is OnNextActionClick -> onNextActionClick(
                event.onUserDataValueChange,
                event.onCallMutationUpdateUserRegisterUseCase
            )
            is OnValidateDocument -> validateDocument(event.email)
            is UIEvent.OnValidateForm -> isFormValid()
        }
    }

    sealed class UIEvent {
        data class OnStart(
            val nationality: String,
            val identificationValue: String,
            val identificationType: String,
            val firstName: String,
            val secondName: String,
            val firstLastName: String,
            val secondLastName: String,
            val fullName: String
        ) : UIEvent()

        data class OnNationalityChange(
            val nationality: String,
            val updateNationality: (nationality: String) -> Unit
        ) :
            UIEvent()

        data class OnFirstNameChange(
            val firstName: String,
            val onSharedViewModelFirstNameChange: () -> Unit
        ) : UIEvent()

        data class OnSecondNameChange(
            val secondName: String,
            val onSharedViewModelSecondNameChange: () -> Unit
        ) : UIEvent()

        data class OnFirstLastNameChange(
            val firstLastName: String,
            val onSharedViewModelFirstLastNameChange: () -> Unit
        ) : UIEvent()

        data class OnSecondLastNameChange(
            val secondLastName: String,
            val onSharedViewModelSecondLastNameChange: () -> Unit
        ) : UIEvent()

        data class OnIdentificationTypeValueChange(val identificationType: String) : UIEvent()
        data class OnIdentificationValueChange(
            val identification: String,
            val identificationShareViewModelChange: () -> Unit
        ) : UIEvent()

        data class OnNextActionClick(
            val onUserDataValueChange: () -> Unit,
            val onCallMutationUpdateUserRegisterUseCase: () -> Unit
        ) : UIEvent()

        data class OnValidateDocument(
            val email: String? = null
        ) : UIEvent()

        object OnValidateForm : UIEvent()
    }

    sealed class BaseEvent {
        data class OnFormValidateCompleted(val isFormValid: Boolean) : BaseEvent()
    }

    companion object {
        const val DUI_VERIFICATION_MODULE = 10
    }
}