package com.multimoney.multimoney.presentation.util

enum class VisualTransformationMasks(val mask: String,val maskChar: Char) {
    PHONE_TRANSFORMATION_MASK("+### #### ####", '#')
}