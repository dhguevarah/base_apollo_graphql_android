package com.multimoney.domain.model.balance

data class Account(
    val totalBalance: Double?,
    val gainedInterest: Double?
)