package com.multimoney.domain.model.balance

data class BalanceAccountSmart(
    val account: List<Account>?
)