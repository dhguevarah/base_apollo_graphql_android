package com.multimoney.domain.model.security

data class ClientInfoCr(
    var fullName: String,
    val status: Boolean
)