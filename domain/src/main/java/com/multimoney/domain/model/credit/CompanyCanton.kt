package com.multimoney.domain.model.credit

data class CompanyCanton(val id: Int, val description: String, val subOptions: List<CatalogSubOptions?>)
