package com.multimoney.domain.model.credit

data class PaymentAmount(
    val paymentAmount: Double,
    val paymentAmountLabel: String
)
