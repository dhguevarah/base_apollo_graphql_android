package com.multimoney.domain.model.security

data class InfoCrypto(
    val status: Int
)
