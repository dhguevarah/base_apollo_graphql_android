package com.multimoney.domain.model.credit

data class CompanyProvince(val id: Int, val description: String, val subOptions: List<CatalogSubOptions?>)
