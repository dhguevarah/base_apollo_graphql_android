package com.multimoney.domain.model.credit

data class CompanyDistrict(val id: Int, val description: String, val subOptions: List<CatalogSubOptions?>)
