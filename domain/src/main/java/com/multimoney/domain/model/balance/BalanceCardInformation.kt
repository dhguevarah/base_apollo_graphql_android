package com.multimoney.domain.model.balance

data class BalanceCardInformation(
    val cardInformation: CardInformation?,
)