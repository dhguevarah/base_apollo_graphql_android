package com.multimoney.domain.model.credit

data class CreditOfferAndTip(
    val id: String,
    val title: String,
    val description: String,
    val actionName: String,
    val icon:String,
    val type: String
)
