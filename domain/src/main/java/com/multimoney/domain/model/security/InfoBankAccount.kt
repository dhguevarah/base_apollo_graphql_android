package com.multimoney.domain.model.security

data class InfoBankAccount(
    val statusFirm: String?,
    val statusOnfido: String?
)