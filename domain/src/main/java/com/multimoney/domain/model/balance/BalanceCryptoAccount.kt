package com.multimoney.domain.model.balance

data class BalanceCryptoAccount(
    val globalBalance: Double?,
)