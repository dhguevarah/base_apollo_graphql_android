package com.multimoney.domain.model.security

data class OnfidoToken(
    val applicantId: String,
    val sdkToken: String
)
