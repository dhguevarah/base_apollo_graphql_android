package com.multimoney.domain.model.security

data class InfoVirtualCard(
    val status: Int
)