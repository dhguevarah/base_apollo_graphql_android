package com.multimoney.domain.model.balance

data class CardInformation(
    val cardNumber: String?,
)