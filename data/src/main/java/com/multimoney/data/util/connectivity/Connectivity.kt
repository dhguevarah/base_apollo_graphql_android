package com.multimoney.data.util.connectivity

interface Connectivity {
    fun hasNetworkAccess(): Boolean
}
